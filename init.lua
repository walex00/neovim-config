---------------------------------------------------
--   _   _       _             ___ ____  _____   --
--  | \ | |_   _(_)_ __ ___   |_ _|  _ \| ____|  --
--  |  \| \ \ / / | '_ ` _ \   | || | | |  _|    --
--  | |\  |\ V /| | | | | | |  | || |_| | |___   --
--  |_| \_| \_/ |_|_| |_| |_| |___|____/|_____|  --
--                                               --
---------------------------------------------------
require "config.settings"
require "config.keymaps"
require "config.lazy"
require "config.colorschemes"
require "config.treesitter"
require "config.colorizer"
require "config.lsp"
require "config.gitsigns"
require "config.lualine"
require "config.toggleterm"
