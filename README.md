# NeoVim IDE Set-Up

## Prerequisites

## Plug-ins

### General

### Git
 
### Treesitter
 
### Telescope
 
### Completion
 
### Snippets
 
### LSP (language Server Protocol)
 
### DAP (Debug Adapter Protocol)
 
### Color Themes
