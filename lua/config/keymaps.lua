-- Modes:
-------------------------------------------------------------------------------
--  normal_mode       = "n",
--  insert_mode       = "i",
--  visual_mode       = "v",
--  visual_block_mode = "x",
--  term_mode         = "t",
--  command_mode      = "c",

local keymap    = vim.keymap.set
local opts      = { noremap = true, silent = true }
local term_opts = { silent = true }

-- Map Leader to <Space>
-------------------------------------------------------------------------------
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader      = " "
vim.g.maplocalleader = " "

-- Ctrl + s to save
-------------------------------------------------------------------------------
keymap("n", "<C-s>", ":w<CR>",       opts)
keymap("i", "<C-s>", "<Esc>:w<CR>a", opts)

-- Ctrl + q to close window
-------------------------------------------------------------------------------
keymap("n", "<C-q>", ":q<CR>", opts)

-- Shift + q to close buffers, not windows
-------------------------------------------------------------------------------
keymap("n", "<S-q>", "<cmd>Bdelete!<CR>", opts)

-- CyBu (Cycle Buffers)
-------------------------------------------------------------------------------
keymap("n", "[b",      ":CybuPrev<CR>",         opts)
keymap("n", "]b",      ":CybuNext<CR>",         opts)
keymap("n", "<s-tab>", ":CybuLastusedPrev<CR>", opts)
keymap("n", "<tab>",   ":CybuLastusedNext<CR>", opts)

-- JABS (Just Another Buffer Switcher)
-------------------------------------------------------------------------------
keymap("n", ",b", ":JABSOpen<CR>", opts)

-- JAQ (Just Another Quickrun)
-------------------------------------------------------------------------------
keymap("n", "[r", ":Jaq<CR>", opts)

-- Ctrl + hjkl to navigate splits
-------------------------------------------------------------------------------
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-l>", "<C-w>l", opts)

-- Resize windows with arrows
-------------------------------------------------------------------------------
keymap("n", "<C-Up>",    ":resize -2<CR>",          opts)
keymap("n", "<C-Down>",  ":resize +2<CR>",          opts)
keymap("n", "<C-Left>",  ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

-- Press jk/kj fast to swicth to NORMAL mode
-------------------------------------------------------------------------------
keymap("i", "jk", "<ESC>", opts)
keymap("i", "kj", "<ESC>", opts)

-- Clear highlights
-------------------------------------------------------------------------------
keymap("n", "<leader>h", "<cmd>nohlsearch<CR>", opts)

-- Nvim-Tree
-------------------------------------------------------------------------------
keymap("n", "<leader>n", ":NvimTreeToggle<CR>", opts)

-- Telescope
-------------------------------------------------------------------------------
keymap("n", "<leader>ff", ":Telescope find_files<CR>", opts)
keymap("n", "<leader>fg", ":Telescope live_grep<CR>",  opts)
keymap("n", "<leader>fb", ":Telescope buffers<CR>",    opts)
keymap("n", "<leader>fp", ":Telescope projects<CR>",   opts)
keymap("n", "<leader>fh", ":Telescope help_tags<CR>",  opts)
--keymap("n", "<leader>ld", ":Telescope diagnostics bufnr=0 theme=get_ivy<CR>",  opts)

-- DAP
-------------------------------------------------------------------------------
keymap("n", "<leader>db", "<cmd>lua require'dap'.toggle_breakpoint()<cr>", opts)
keymap("n", "<leader>dc", "<cmd>lua require'dap'.continue()<cr>", opts)
keymap("n", "<leader>di", "<cmd>lua require'dap'.step_into()<cr>", opts)
keymap("n", "<leader>do", "<cmd>lua require'dap'.step_over()<cr>", opts)
keymap("n", "<leader>dO", "<cmd>lua require'dap'.step_out()<cr>", opts)
keymap("n", "<leader>dr", "<cmd>lua require'dap'.repl.toggle()<cr>", opts)
keymap("n", "<leader>dl", "<cmd>lua require'dap'.run_last()<cr>", opts)
keymap("n", "<leader>du", "<cmd>lua require'dapui'.toggle()<cr>", opts)
keymap("n", "<leader>dt", "<cmd>lua require'dap'.terminate()<cr>", opts)

-- LazyGit
-------------------------------------------------------------------------------
keymap("n", "<leader>gg", "<cmd>lua _LAZYGIT_TOGGLE()<CR>", opts)

-- Comment
-------------------------------------------------------------------------------
--keymap("n", "<leader>/", "<cmd>lua require('Comment.api').toggle_current_linewise()<CR>", opts)
keymap("n", "<leader>/", "<CMD>lua require('Comment.api').toggle.linewise.current()<CR>", opts)
-- keymap("x", "<leader>/", '<ESC><CMD>lua require("Comment.api").toggle_linewise_op(vim.fn.visualmode())<CR>')
keymap("x", "<leader>/", '<ESC><CMD>lua require("Comment.api").toggle.linewise(vim.fn.visualmode())<CR>')
