vim.opt.title          = true                         -- 
vim.opt.number         = false                        -- Activate numbered lines
vim.opt.relativenumber = true                         -- Show relative line numbers
vim.opt.cursorline     = true                         -- Highlight the current line 
vim.opt.showmode       = false                        -- Disable mode text
vim.opt.clipboard:prepend {'unnamed', 'unnamedplus'}  -- Allows neovim to access the system clipboard
vim.opt.conceallevel   = 0                            -- `` are visible in Markdown files
vim.opt.showtabline    = 0                            -- Always show tabs
vim.opt.cmdheight      = 1
vim.opt.hidden         = true
vim.opt.ruler          = false
vim.opt.showcmd        = false
vim.opt.splitbelow     = true
vim.opt.splitright     = true
vim.opt.mouse          = "a"
vim.opt.hlsearch       = false
vim.opt.incsearch      = true
vim.opt.ignorecase     = true
vim.opt.smartcase      = true
vim.opt.smartindent    = true
vim.opt.expandtab      = true
vim.opt.smarttab       = true
vim.opt.tabstop        = 2
vim.opt.numberwidth    = 4                  -- Set number column width to 2 {default 4}
vim.opt.shiftwidth     = 2
vim.opt.fileencoding   = 'utf-8'
vim.opt.termguicolors  = true
vim.opt.winblend       = 0
vim.opt.wildoptions    = 'pum'
vim.opt.pumblend       = 5
vim.opt.background     = 'dark'
vim.opt.scrolloff      = 8
vim.opt.sidescrolloff  = 8
vim.opt.wrap           = false
vim.opt.signcolumn     = "yes"
vim.opt.swapfile       = false
vim.opt.backup         = false
vim.opt.writebackup    = false
vim.opt.undofile       = true
vim.opt.guifont        = "monospace:h17"    -- The font used in graphical Neovim applications
vim.opt.fillchars.eob  = " "
vim.opt.shortmess:append "c"
vim.opt.path:append { '**' }                -- Search down into sub-folders

vim.g.loaded_netrw       = 1
vim.g.loaded_netrwPlugin = 1

vim.cmd "set whichwrap+=<,>,[,],h,l"
vim.cmd [[set iskeyword+=-]]

-- Highlight yanked text for 200ms
vim.cmd [[
  augroup highlight_yank
  autocmd!
  au TextYankPost * silent! lua vim.highlight.on_yank({higroup="Visual", timeout=100})
  augroup END
]]
