local M = {}

-- Setup
-------------------------------------------------------------------------------
M.setup = function()
  local signs = {
    { name = "DiagnosticSignError", text = "" },
    { name = "DiagnosticSignWarn",  text = "" },
    { name = "DiagnosticSignHint",  text = "" },
    { name = "DiagnosticSignInfo",  text = "" },
  }

  for _, sign in ipairs(signs) do
    vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.text, numhl = "" })
  end

  local config = {
    virtual_text = false, -- Disable virtual text
    signs = {
      active = signs,   -- Show signs
    },
    update_in_insert = true,
    underline = true,
    severity_sort = true,
    float = {
      focusable = true,
      close_events = { "BufLeave", "CursorMoved", "InsertEnter", "FocusLost" },
      style = "minimal",
      border = "rounded",
      source = "always",
      header = "",
      prefix = "",
    },
  }

  vim.diagnostic.config(config)

  vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
    border = "rounded",
  })

  vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
    border = "rounded",
  })
end

-- on_attach
-------------------------------------------------------------------------------
M.on_attach = function(client, bufnr)
  local keymap = vim.keymap.set
  local opts = { noremap = true, silent = true }

  keymap("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
  keymap("n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", opts)                       -- Jumps to definition of the symbol under the cursor
  keymap("n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)                   -- Lists all the implementations for the symbol under cursor in quickfix window
  keymap("n", "gr", "<cmd>lua vim.lsp.buf.references()<CR>", opts)                       -- lists all references for symbol under the cursor in quickfix window
  keymap("n", "<leader>lk", "<cmd>lua vim.diagnostic.goto_prev()<CR>", opts)             -- Jumps to previous diagnostic
  keymap("n", "<leader>lj", "<cmd>lua vim.diagnostic.goto_next()<CR>", opts)             -- Jumps to next diagnostic
  keymap("n", "<leader>lh", "<cmd>lua vim.lsp.buf.hover()<CR>", opts)                    -- Information about the symbol under cursor
  keymap("n", "<leader>ld", "<cmd>lua vim.diagnostic.open_float()<CR>", opts)            -- Displays diagnostics in floating window
  keymap("n", "<leader>lq", "<cmd>lua vim.diagnostic.setloclist()<CR>", opts)            -- Display all diagnostics in quickfix window
  keymap("n", "<leader>lf", "<cmd>lua vim.lsp.buf.format()<CR>", opts)                   -- Format the current buffer
  keymap("n", "<leader>lr", "<cmd>lua vim.lsp.util.rename()<CR>", opts)                  -- Renaname old_fname to new_fname
  keymap("n", "<leader>la", "<cmd>lua vim.lsp.buf.code_action()<CR>", opts)              -- Selects a code action available at the current cursor position
  keymap("n", "<leader>li", "<cmd>LspInfo<cr>", opts)                                    -- LSP Information
  keymap("n", "<leader>ls", "<cmd>lua vim.lsp.buf.signature_help()<cr>", opts)           -- Signature Help
  keymap("n", "<leader>lD", "<cmd>Telescope diagnostics bufnr=0 theme=get_ivy<cr>", opts) -- Buffer Diagnostics
  keymap("n", "<leader>lw", "<cmd>Telescope diagnostics<cr>", opts)                      -- Diagnostics
end

return M
