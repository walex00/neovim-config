local status_ok, mason_lspconfig, lspconfig

-- Null-LS
-------------------------------------------------------------------------------
require "config.lsp.null-ls"

-- Mason
-------------------------------------------------------------------------------
require "config.lsp.mason"

-- Mason-LSPconfig
-------------------------------------------------------------------------------
status_ok, mason_lspconfig = pcall(require, "mason-lspconfig")
if not status_ok then
    return
end

mason_lspconfig.setup {
    -- A list of servers to automatically install if they're not already installed.
    -- Example: { "rust_analyzer@nightly", "lua_ls" }
    -- This setting has no relation with the `automatic_installation` setting.
    ---@type string[]
    ensure_installed = { "lua_ls", "clangd", "bashls" },

    -- Whether servers that are set up (via lspconfig) should be automatically
    -- installed if they're not already installed.
    -- This setting has no relation with the `ensure_installed` setting.
    -- Can either be:
    --   - false: Servers are not automatically installed.
    --   - true: All servers set up via lspconfig are automatically installed.
    --   - { exclude: string[] }: All servers set up via lspconfig, except the ones
    -- provided in the list, are automatically installed.
    --       Example: automatic_installation = { exclude = { "rust_analyzer", "solargraph" } }
    ---@type boolean
    automatic_installation = false,

    -- See `:h mason-lspconfig.setup_handlers()`
    ---@type table<string, fun(server_name: string)>?
    handlers = nil,
}

-- LSPconfig
-------------------------------------------------------------------------------
status_ok, lspconfig = pcall(require, "lspconfig")
if not status_ok then
    return
end

-- Load LSP Handlers
-------------------------------------------------------------------------------
require("config.lsp.handlers").setup()
local on_attach = require("config.lsp.handlers").on_attach

-- Lua
-------------------------------------------------------------------------------
lspconfig.lua_ls.setup {
    on_attach = on_attach,
    settings  = require "config.lsp.settings.lua_ls"
}

-- Clangd
-------------------------------------------------------------------------------
lspconfig.clangd.setup {
    on_attach = on_attach,
    settings  = require "config.lsp.settings.clangd"
}

-- Bash
-------------------------------------------------------------------------------
lspconfig.bashls.setup {
    on_attach = on_attach,
}
