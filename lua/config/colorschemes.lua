-- Change colorscheme here
-------------------------------------------------------------------------------
-- local colorscheme = "onedark"
-- local colorscheme = "gruvbox-baby"
-- local colorscheme = "dracula"
-- local colorscheme = "molokai"
-- local colorscheme = "solarized"
-- local colorscheme = "tokyonight-night"  -- night, storm, day, moon
local colorscheme = "catppuccin"

-- Global Settings
-------------------------------------------------------------------------------
-- vim.g.transparent_background = true        -- transparent background(Default: false)
-- vim.g.italic_comments = true               -- italic comments(Default: true)
-- vim.g.italic_keywords = true               -- italic keywords(Default: true)
-- vim.g.italic_functions = true              -- italic functions(Default: false)
-- vim.g.italic_variables = true              -- italic variables(Default: false

-- Catppuccin Setup (Flavours = latte, frappe, macchiato, mocha)
-------------------------------------------------------------------------------
vim.g.catppuccin_flavour = "macchiato"

-- Gruvbox-Baby Setup
-------------------------------------------------------------------------------
vim.g.gruvbox_baby_function_style   = "NONE"   -- NONE, italic, bold, etc
vim.g.gruvbox_baby_keyword_style    = "NONE"   -- NONE, italic, bold, etc
vim.g.gruvbox_baby_background_color = "medium" -- medium, dark
vim.g.gruvbox_baby_telescope_theme  = 1
vim.g.gruvbox_baby_transparent_mode = 0

-- Set Colorscheme
-------------------------------------------------------------------------------
local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not status_ok then
  vim.notify("colorscheme " .. colorscheme .. " not found!")
  return
end
