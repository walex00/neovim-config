return {

    -- General
    --------------------------------------------------------------------------- 
    "kyazdani42/nvim-web-devicons",                              -- Web Dev Icons
    "nvim-lualine/lualine.nvim",                                 -- Lualine
    "NvChad/nvim-colorizer.lua",                                 -- NvChad Colorizer
    "akinsho/toggleterm.nvim",                                   -- ToggleTerm

    -- Treesitter
    --------------------------------------------------------------------------- 
    { "nvim-treesitter/nvim-treesitter", build = ":TSUpdate" },  -- Treesitter
    "nvim-treesitter/nvim-treesitter-textobjects",               -- Syntax aware text-objects, select, move, swap, and peek support
    "p00f/nvim-ts-rainbow",                                      -- Rainbow parentheses for TS

    -- Git
    --------------------------------------------------------------------------- 
    "lewis6991/gitsigns.nvim",                                   -- Gitsigns

    -- LSP (Language Server Protocol)
    --------------------------------------------------------------------------- 
    "williamboman/mason.nvim", build = ":MasonUpdate",           -- Mason LSP Manager
    "williamboman/mason-lspconfig.nvim",                         -- Mason LSP Config
    "neovim/nvim-lspconfig",                                     -- LSP Config
    "jose-elias-alvarez/null-ls.nvim",                           -- Formatters and Linters
    "nvim-lua/plenary.nvim",                                     -- Additional Functions

    -- Color Themes
    --------------------------------------------------------------------------- 
    "Mofiqul/dracula.nvim",                                      -- Dracula
    "catppuccin/nvim",                                           -- Catppuccin
    "lunarvim/colorschemes",                                     -- LunarVim
    "ishan9299/nvim-solarized-lua",                              -- Solarized
    "tomasr/molokai",                                            -- Molokai
    "luisiacc/gruvbox-baby",                                     -- Gruvbox
    "folke/tokyonight.nvim",                                     -- Tokyo Night

}
